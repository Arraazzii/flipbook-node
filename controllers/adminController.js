const { sequelize } = require('../models/db.js');

const jwt_decode  = require("jwt-decode");
const express     = require('express');
const app         = express();
const secret      = require('../config/secret.js');
const moment      = require('moment');
const md5         = require('md5');
const path        = require("path");
const root        = path.dirname(require.main.filename || process.mainModule.filename);
const readXlsxFile = require('read-excel-file/node')

const fs          = require('fs');

const { v4 : uuidv4 }   = require('uuid');
const d                 = Date(Date.now());
// const now               = d.toString();

//Get Tools from toolsController
const tools = require('./toolsController.js');
var now = tools.getDateTimeNow();

app.set('superSecret', secret.code);

exports.getUser = async (req, res) => {
    // // GET JWT TOKEN
    // const jwttoken = JSON.stringify(req.headers.authorization);

    // // DECODE JWT TOKEN
    // const decoded  = jwt_decode(jwttoken);
    
    // var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    // var result  = await sequelize.query(query, {
    //     replacements: [decoded.code, 'Admin', '1'],
    //     type: sequelize.QueryTypes.SELECT,
    // });

    // if (!result) {
    //     return res.status(500).json({
    //         error: true,
    //         response: "User Not Found/Not Active!",
    //     });
    // }

    const code      = req.query.code;
    const role      = req.query.role;
    const username  = req.query.username;
    var where  = "";

    if (code != undefined && code != "") {
        where += `AND a.code = '${code}'`;
    }
    
    if (role != undefined && role != "") {
        where += `AND role = '${role}'`;
    }
    
    if (username != undefined && username != "") {
        where += `AND b.username = '${username}'`;
    }

    query  = `SELECT a.*, b.username FROM public.users AS a JOIN public.auth AS b ON b.code = a.code WHERE 1=1 ${where}`;
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.addUser = async (req, res) => {
    now = tools.getDateTimeNow();

    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);
    // DECODE JWT TOKEN
    var decoded = jwt_decode(jwttoken);

    // const code = await tools.getMasterCode("public.users", "code", "USER");
    const code = req.body.nisn;
    const first_name = req.body.first_name;
    const last_name = req.body.last_name;
    const profile = req.body.profile;
    const role = req.body.role;
    const username = req.body.nisn;
    const password = md5('12345');
    const status = req.body.status;

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    const validateUsername = await tools.checkUsername(username);
    if (!validateUsername) {
        return res.status(200).json({
            error: true,
            response: "NISN Already Exist!"
            // suggestions: generateUsername(first_name, last_name)
        });
    }

    var query   = `INSERT INTO public.users (code, first_name, last_name, profile, role, status, createdby, createdtime) VALUES(?, ?, ?, ?, ?, ?, ?, ?)`;
    var result  = await sequelize.query(query, {
        replacements: [code, first_name, last_name, profile, role, status, decoded.code, now],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        query   = `INSERT INTO public.auth (code, username, password) VALUES (?, ?, ?)`;
        result  = await sequelize.query(query, {
            replacements: [code, username, password],
            type: sequelize.QueryTypes.INSERT,
        });

        if (result) {
            let data    = [code, first_name, last_name, profile, role, status, decoded.code, now];
            const log   = await tools.addLog("Flipbook|User", "Insert", JSON.stringify(data), decoded.code);
            
            if (log){
                return res.status(200).json({
                    error: false,
                    response: "Add Data Success!",
                });
            }
        }
    }

    return res.status(500).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
};

exports.addUserBulk = async (req, res) => {
    now = tools.getDateTimeNow();

    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);
    // DECODE JWT TOKEN
    var decoded = jwt_decode(jwttoken);

    const type = req.body.type;
    const password = md5('12345');

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    var query = "";
    var result = "";

    if (req.files) {
        var file = req.files.berkas != null ? req.files.berkas : null;
        var filename = uuidv4();
        file.mv(root + "/public/upload/excel/" + filename + ".xlsx", function(err) {
            if (err) {
                return res.status(500).json({
                    error: true,
                    response: "Error When Upload Excel!",
                });
            }
        });

        readXlsxFile(root + "/public/upload/excel/" + filename + ".xlsx").then((rows) => {
            rows.shift();

            var rn = 0;
            rows.forEach(row => {
                let datachild = new Array();
                query   = `INSERT INTO public.users (code, first_name, last_name, profile, role, status, createdby, createdtime) VALUES(?, ?, ?, ?, ?, ?, ?, ?)`;
                result  = sequelize.query(query, {
                    replacements: [row[0], row[1], row[2], "", type, 1, decoded.code, now],
                    type: sequelize.QueryTypes.INSERT,
                });

                datachild['user'] = [row[0], row[1], row[2], "", type, 1, decoded.code, now];

                if(result){
                    query   = `INSERT INTO public.auth (code, username, password) VALUES (?, ?, ?)`;
                    result  = sequelize.query(query, {
                        replacements: [row[0], row[0], password],
                        type: sequelize.QueryTypes.INSERT,
                    });

                    datachild['auth'] = [row[0], row[0], password];

                    // tools.addLog("Flipbook|User", "Insert", JSON.stringify(datachild), decoded.code);
                }

                rn++;
            });
        });

        // let log = await tools.addLog("Flipbook|User", "Insert Bulk", JSON.stringify(dataParent), decoded.code);
        // if (log){
            return res.status(200).json({
                error: false,
                response: "Add Data Success!",
            });
        // }
    }
};

exports.updateUser = async (req, res) => {
    now = tools.getDateTimeNow();
    
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }
    
    const code = req.body.code;
    const first_name = req.body.first_name;
    const last_name = req.body.last_name;
    const profile = req.body.profile;
    const status = req.body.status;

    query       = `SELECT * FROM public.users WHERE code = ?`;
    var before  = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.SELECT,
    });

    query  = `UPDATE public.users SET first_name = ?, last_name = ?, profile = ?, status = ?, updatedby = ?, updatedtime = ? WHERE code = ?`;
    result = await sequelize.query(query, {
        replacements: [first_name, last_name, profile, status, decoded.code, now, code],
        type: sequelize.QueryTypes.UPDATE,
    });

    if(result){
        query       = `SELECT * FROM public.users WHERE code = ?`;
        var after   = await sequelize.query(query, {
            replacements: [code],
            type: sequelize.QueryTypes.SELECT,
        });

        let data = {
            before:before,
            after:after
        };
        const log   = await tools.addLog("Flipbook|User", "Update", JSON.stringify(data), decoded.code);
            
        if (log){
            return res.status(200).json({
                error: false,
                response: "Update Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Update Data Failed. Please Try Again!",
    })
};

exports.deleteUser = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    // PARAMS
    const code = req.query.code;

    query       = `SELECT * FROM public.users AS a LEFT JOIN public.auth AS b ON b.code = a.code WHERE a.code = ?`;
    var result1 = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.SELECT,
    });

    query  = `DELETE FROM public.users WHERE code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });
    
    if(result){
        query  = `DELETE FROM public.auth WHERE code = ?`;
        result = await sequelize.query(query, {
            replacements: [code],
            type: sequelize.QueryTypes.DELETE,
        });

        if (result) {
            let data    = result1;
            const log   = await tools.addLog("Flipbook|User", "Delete", JSON.stringify(data), decoded.code);
            
            if (log){
                return res.status(200).json({
                    error: false,
                    response: "Delete Data Success!",
                });
            }
        }   
    }

    return res.status(500).json({
        error: true,
        response: "Delete Data Failed. Please Try Again!",
    })
};

exports.getClass = async (req, res) => {
    // // GET JWT TOKEN
    // const jwttoken = JSON.stringify(req.headers.authorization);

    // // DECODE JWT TOKEN
    // const decoded  = jwt_decode(jwttoken);
    
    // var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    // var result  = await sequelize.query(query, {
    //     replacements: [decoded.code, 'Admin', '1'],
    //     type: sequelize.QueryTypes.SELECT,
    // });

    // if (!result) {
    //     return res.status(500).json({
    //         error: true,
    //         response: "User Not Found/Not Active!",
    //     });
    // }

    const code = req.query.code;
    var where = "";

    if (code != undefined && code != "") {
        where += `AND code = '${code}'`;
    }

    query  = `SELECT * FROM flipbook.class WHERE 1=1 ${where} ORDER BY createdtime DESC`;
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.addClass = async (req, res) => {
    now = tools.getDateTimeNow();

    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);
    // DECODE JWT TOKEN
    var decoded = jwt_decode(jwttoken);

    const code   = await tools.getMasterCode("flipbook.class", "code", "CLS");
    const name   = req.body.name;
    const status = req.body.status;
    var filename = null;

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    if (req.files) {
        let thumbnails = req.files.thumbnails != null ? req.files.thumbnails : null;

        if (thumbnails) {
            if (thumbnails.mimetype == "image/png" || thumbnails.mimetype == "image/jpg" || thumbnails.mimetype == "image/jpeg") {
                filename = code.replaceAll("/", "");
                thumbnails.mv(root + "/public/upload/class/" + code.replaceAll("/", "") + "/thumbnails/" + filename + ".png", function(err) {
                    if (err) {
                        return res.status(500).json({
                            error: true,
                            response: "Error When Upload Thumbnails!",
                        });
                    }
                });
            } else {
                return res.status(500).json({
                    error: true,
                    response: "File Type Not Accept!",
                });
            }
        }
    }

    var query   = `INSERT INTO flipbook.class (code, name, status, thumbnails, createdby, createdtime) VALUES(?, ?, ?, ?, ?, ?)`;
    var result  = await sequelize.query(query, {
        replacements: [code, name, status, filename, decoded.code, now],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        let data    = [code, name, status, filename, decoded.code, now];
        const log   = await tools.addLog("Flipbook|Class", "Insert", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Add Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
};

exports.updateClass = async (req, res) => {
    now = tools.getDateTimeNow();

    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }
    
    const code    = req.body.code;
    const name    = req.body.name;
    const status  = req.body.status;
    var filename  = null;

    query       = `SELECT * FROM flipbook.class WHERE code = ?`;
    var before  = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.SELECT,
    });

    if(req.files){
        let thumbnails = req.files.thumbnails != null ? req.files.thumbnails : null;
        filename = code.replaceAll("/", "");
        if(thumbnails){
            if (before[0].thumbnails != "" && before[0].thumbnails != null) {
                fs.rmSync(root + "/public/upload/class/" + code.replaceAll("/", "") + "/thumbnails/" + code.replaceAll("/", "") + ".png", { recursive: true });
            }

            if (thumbnails.mimetype == "image/png" || thumbnails.mimetype == "image/jpg" || thumbnails.mimetype == "image/jpeg") {
                thumbnails.mv(root + "/public/upload/class/" + code.replaceAll("/", "") + "/thumbnails/" + filename + ".png", function(err) {
                    if (err) {
                        return res.status(500).json({
                            error: true,
                            response: "Error When Upload Thumbnails!",
                        });
                    }
                });
            } else {
                return res.status(500).json({
                    error: true,
                    response: "File Type Not Accept!",
                });
            }
        }
    }
    
    query  = `UPDATE flipbook.class SET name = ?, status = ?, updatedby = ?, updatedtime = ?, thumbnails = ? WHERE code = ?`;
    result = await sequelize.query(query, {
        replacements: [name, status, decoded.code, now, filename, code],
        type: sequelize.QueryTypes.UPDATE,
    });

    if(result){
        query       = `SELECT * FROM flipbook.class WHERE code = ?`;
        var after   = await sequelize.query(query, {
            replacements: [code],
            type: sequelize.QueryTypes.SELECT,
        });

        let data = {
            before:before,
            after:after
        };
        const log   = await tools.addLog("Flipbook|Class", "Update", JSON.stringify(data), decoded.code);
            
        if (log){
            return res.status(200).json({
                error: false,
                response: "Update Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Update Data Failed. Please Try Again!",
    })
};

exports.deleteClass = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    // PARAMS
    const code = req.query.code;

    query       = `SELECT * FROM flipbook.class WHERE code = ?`;
    var result1 = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.SELECT,
    });

    if (result1[0].thumbnails != "" && result1[0].thumbnails != null) {
        fs.rmSync(root + "/public/upload/class/" + code.replaceAll("/", "") + "/thumbnails/" + code.replaceAll("/", "") + ".png", { recursive: true });
    }

    query  = `DELETE FROM flipbook.class WHERE code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });

    query  = `DELETE FROM flipbook.class_book WHERE class_code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });
    
    query  = `DELETE FROM flipbook.class_user WHERE class_code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });
    
    if(result){
        let data    = result1;
        const log   = await tools.addLog("Flipbook|Class", "Delete", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Delete Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Delete Data Failed. Please Try Again!",
    })
};

exports.getUserClass = async (req, res) => {
    // // GET JWT TOKEN
    // const jwttoken = JSON.stringify(req.headers.authorization);

    // // DECODE JWT TOKEN
    // const decoded  = jwt_decode(jwttoken);

    // var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    // var result  = await sequelize.query(query, {
    //     replacements: [decoded.code, 'Admin', '1'],
    //     type: sequelize.QueryTypes.SELECT,
    // });

    // if (!result) {
    //     return res.status(500).json({
    //         error: true,
    //         response: "User Not Found/Not Active!",
    //     });
    // }

    const classs = req.query.classs;
    const role = req.query.role;
    const user = req.query.user;
    var where = "";

    if (classs != undefined && classs != "") {
        where += `AND a.class_code = '${classs}'`;
    }
    
    if (role != undefined && role != "") {
        where += `AND b.role = '${role}'`;
    }
    
    if (user != undefined && user != "") {
        where += `AND a.user_code = '${user}'`;
    }

    query  = `SELECT a.*, b.first_name, b.last_name FROM flipbook.class_user AS a JOIN public.users AS b ON b.code = a.user_code WHERE 1=1 ${where}`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.addUserClass = async (req, res) => {
    now = tools.getDateTimeNow();

    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);
    // DECODE JWT TOKEN
    var decoded = jwt_decode(jwttoken);

    const code   = await tools.getNotransactions("flipbook.class_user", "code", "USC");
    const classs = req.body.class;
    const user   = req.body.user;

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    var query   = `INSERT INTO flipbook.class_user (code, class_code, user_code, createdby, createdtime) VALUES(?, ?, ?, ?, ?)`;
    var result  = await sequelize.query(query, {
        replacements: [code, classs, user, decoded.code, now],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        let data    = [code, classs, user, decoded.code, now];
        const log   = await tools.addLog("Flipbook|User Class", "Insert", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Add Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
};

exports.deleteUserClass = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    // PARAMSs
    const user = req.query.user;
    const classs = req.query.class;

    query       = `SELECT * FROM flipbook.class_user WHERE class_code = ? AND user_code = ?`;
    var result1 = await sequelize.query(query, {
        replacements: [classs, user],
        type: sequelize.QueryTypes.SELECT,
    });

    query  = `DELETE FROM flipbook.class_user WHERE class_code = ? AND user_code = ?`;
    result = await sequelize.query(query, {
        replacements: [classs, user],
        type: sequelize.QueryTypes.DELETE,
    });
    
    if(result){
        let data    = result1;
        const log   = await tools.addLog("Flipbook|User Class", "Delete", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Delete Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Delete Data Failed. Please Try Again!",
    })
};

exports.getBookClass = async (req, res) => {
    // // GET JWT TOKEN
    // const jwttoken = JSON.stringify(req.headers.authorization);

    // // DECODE JWT TOKEN
    // const decoded  = jwt_decode(jwttoken);

    // var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    // var result  = await sequelize.query(query, {
    //     replacements: [decoded.code, 'Admin', '1'],
    //     type: sequelize.QueryTypes.SELECT,
    // });

    // if (!result) {
    //     return res.status(500).json({
    //         error: true,
    //         response: "User Not Found/Not Active!",
    //     });
    // }

    const classs = req.query.class;
    const book = req.query.book;
    var where = "";

    if (classs != undefined && classs != "") {
        where += `AND a.class_code = '${classs}'`;
    }
    
    if (book != undefined && book != "") {
        where += `AND a.book_code = '${book}'`;
    }

    query  = `SELECT a.*, b.title, b.thumbnail FROM flipbook.class_book AS a JOIN flipbook.book AS b ON b.code = a.book_code WHERE b.status = 1 ${where}`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.addBookClass = async (req, res) => {
    now = tools.getDateTimeNow();

    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);
    // DECODE JWT TOKEN
    var decoded = jwt_decode(jwttoken);

    const code   = await tools.getNotransactions("flipbook.class_book", "code", "BOC");
    const classs = req.body.class;
    const book   = req.body.book;

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    var query   = `INSERT INTO flipbook.class_book (code, class_code, book_code, createdby, createdtime) VALUES(?, ?, ?, ?, ?)`;
    var result  = await sequelize.query(query, {
        replacements: [code, classs, book, decoded.code, now],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        let data    = [code, classs, book, decoded.code, now];
        const log   = await tools.addLog("Flipbook|Book Class", "Insert", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Add Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
};

exports.deleteBookClass = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    // PARAMS
    const code = req.query.code;

    query       = `SELECT * FROM flipbook.class_book WHERE code = ?`;
    var result1 = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.SELECT,
    });

    query  = `DELETE FROM flipbook.class_book WHERE code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });
    
    if(result){
        let data    = result1;
        const log   = await tools.addLog("Flipbook|Book Class", "Delete", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Delete Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Delete Data Failed. Please Try Again!",
    })
};

exports.getCategories = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    const code = req.query.code;
    var where  = "";

    if (code != undefined && code != "") {
        where += `AND code = '${code}'`;
    }

    query  = `SELECT * FROM flipbook.categories WHERE 1=1 ${where} ORDER BY createdtime DESC`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.addCategories = async (req, res) => {
    now = tools.getDateTimeNow();

    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);
    // DECODE JWT TOKEN
    var decoded = jwt_decode(jwttoken);

    const code   = await tools.getMasterCode("flipbook.categories", "code", "CTG");
    const name   = req.body.name;
    const status = req.body.status;

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    var query   = `INSERT INTO flipbook.categories (code, name, status, createdby, createdtime) VALUES(?, ?, ?, ?, ?)`;
    var result  = await sequelize.query(query, {
        replacements: [code, name, status, decoded.code, now],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        let data    = [code, name, status, decoded.code, now];
        const log   = await tools.addLog("Flipbook|Categories", "Insert", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Add Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
};

exports.updateCategories = async (req, res) => {
    now = tools.getDateTimeNow();

    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }
    
    const code    = req.body.code;
    const name    = req.body.name;
    const status  = req.body.status;

    query       = `SELECT * FROM flipbook.categories WHERE code = ?`;
    var before  = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.SELECT,
    });
    
    query  = `UPDATE flipbook.categories SET name = ?, status = ?, updatedby = ?, updatedtime = ? WHERE code = ?`;
    result = await sequelize.query(query, {
        replacements: [name, status, decoded.code, now, code],
        type: sequelize.QueryTypes.UPDATE,
    });

    if(result){
        query       = `SELECT * FROM flipbook.categories WHERE code = ?`;
        var after   = await sequelize.query(query, {
            replacements: [code],
            type: sequelize.QueryTypes.SELECT,
        });

        let data = {
            before:before,
            after:after
        };
        const log   = await tools.addLog("Flipbook|Categories", "Update", JSON.stringify(data), decoded.code);
            
        if (log){
            return res.status(200).json({
                error: false,
                response: "Update Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Update Data Failed. Please Try Again!",
    })
};

exports.deleteCategories = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    // PARAMS
    const code = req.query.code;

    query       = `SELECT * FROM flipbook.categories WHERE code = ?`;
    var result1 = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.SELECT,
    });

    query  = `DELETE FROM flipbook.categories WHERE code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });
    
    if(result){
        let data    = result1;
        const log   = await tools.addLog("Flipbook|Categories", "Delete", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Delete Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Delete Data Failed. Please Try Again!",
    })
};

exports.getRatings = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    const book = req.query.book;
    const user = req.query.user;
    var where  = "";

    if (book != undefined && book != "") {
        where += ` AND a.book_code = '${book}'`;
    }
    
    if (user != undefined && user != "") {
        where += ` AND a.createdby = '${user}'`;
    }

    query  = `SELECT a.*, b.first_name, b.last_name FROM flipbook.book_rating AS a LEFT JOIN public.users AS b ON b.code = a.createdby WHERE 1=1${where}`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.addRatings = async (req, res) => {
    now = tools.getDateTimeNow();

    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);
    // DECODE JWT TOKEN
    var decoded = jwt_decode(jwttoken);

    const code          = await tools.getNotransactions("flipbook.book_rating", "code", "BRT");
    const rating        = req.body.rating;
    const book          = req.body.book;
    const descriptions  = req.body.descriptions;

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    var query   = `INSERT INTO flipbook.book_rating (code, rating, book_code, descriptions, createdby, createdtime) VALUES(?, ?, ?, ?, ?, ?)`;
    var result  = await sequelize.query(query, {
        replacements: [code, rating, book, descriptions, decoded.code, now],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        let data    = [code, rating, book, descriptions, decoded.code, now];
        const log   = await tools.addLog("Flipbook|Rating", "Insert", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Add Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
};

exports.deleteRatings = async (req, res) => {
    now = tools.getDateTimeNow();

    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    // PARAMS
    const code = req.query.code;

    query   = `SELECT * FROM flipbook.book_rating WHERE code = ?`;
    var result1 = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.SELECT,
    });

    query  = `DELETE FROM flipbook.book_rating WHERE code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });
    
    if(result){
        let data    = result1;
        const log   = await tools.addLog("Flipbook|Rating", "Delete", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Delete Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Delete Data Failed. Please Try Again!",
    })
};

exports.getComments = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    const parent = req.query.parent;
    var where  = "";

    if (parent != undefined && parent != "") {
        where += `AND parent_code = '${parent}'`;
    }

    query  = `SELECT a.*, b.first_name, b.last_name FROM flipbook.book_comment AS a LEFT JOIN public.users AS b ON b.code = a.createdby WHERE 1=1 ${where}`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.addComments = async (req, res) => {
    now = tools.getDateTimeNow();

    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);
    // DECODE JWT TOKEN
    var decoded = jwt_decode(jwttoken);

    const code          = await tools.getNotransactions("flipbook.book_comment", "code", "CMN");
    const parent        = req.body.parent;
    const book          = req.body.book;
    const comment       = req.body.comment;

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    var query   = `INSERT INTO flipbook.book_comment (code, parent_code, book_code, comment, createdby, createdtime) VALUES(?, ?, ?, ?, ?, ?)`;
    var result  = await sequelize.query(query, {
        replacements: [code, parent, book, comment, decoded.code, now],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        let data    = [code, parent, book, comment, decoded.code, now];
        const log   = await tools.addLog("Flipbook|Comment", "Insert", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Add Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
};

exports.deleteComments = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    // PARAMS
    const code = req.query.code;

    query       = `SELECt * FROM flipbook.book_comment WHERE code = ?`;
    var result1 = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.SELECT,
    });

    query  = `DELETE FROM flipbook.book_comment WHERE code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });
    
    if(result){
        let data    = result1;
        const log   = await tools.addLog("Flipbook|Comment", "Delete", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Delete Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Delete Data Failed. Please Try Again!",
    })
};

exports.getBooks = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    const code = req.query.code;
    const categories = req.query.categories;
    var where  = "";

    if (code != undefined && code != "") {
        where += `AND a.code = '${code}'`;
    }
    
    if (categories != undefined && categories != "") {
        where += `AND a.categories = '${categories}'`;
    }

    query  = `SELECT a.*, b.name AS category_name FROM flipbook.book AS a JOIN flipbook.categories AS b ON b.code = a.categories WHERE 1=1 ${where} ORDER BY a.createdtime DESC`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.addBooks = async (req, res) => {
    now = tools.getDateTimeNow();

    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);
    // DECODE JWT TOKEN
    var decoded = jwt_decode(jwttoken);

    const code   = await tools.getNotransactions("flipbook.book", "code", "BOK");
    const title   = req.body.title;
    const categories = req.body.categories;
    const status = req.body.status;
    const sinopsis = req.body.sinopsis;
    const publisher = req.body.publisher;
    const yearPublish = req.body.yearPublish;
    

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    var background;
    var berkas;
    var filename    = null;
    var filename1   = null;
    var filename2   = null;
    var thumbnails;
    var uid = uuidv4();

    if (req.files) {
        background  = req.files.background != null ? req.files.background : null;
        berkas      = req.files.berkas != null ? req.files.berkas : null;
        thumbnails  = req.files.thumbnails != null ? req.files.thumbnails : null;

        if (berkas) {
            if (berkas.mimetype == "application/pdf") {
                filename = "Books-"+uid;
                berkas.mv(root + "/public/upload/books/" + filename + "/" + filename + ".pdf", function(err) {
                    if (err) {
                        return res.status(500).json({
                            error: true,
                            response: "Error When Upload Books!",
                        });
                    }
                });

                if (background) {
                    if (background.mimetype == "image/png" || background.mimetype == "image/jpg" || background.mimetype == "image/jpeg") {
                        filename1 = "background-"+uid+".png";
                        background.mv(root + "/public/upload/books/" + filename + "/background/" + filename1, function(err) {
                            if (err) {
                                return res.status(500).json({
                                    error: true,
                                    response: "Error When Upload Background!",
                                });
                            }
                        });
                    } else {
                        return res.status(500).json({
                            error: true,
                            response: "File Type Not Accept!",
                        });
                    }
                }

                if (thumbnails) {
                    if (thumbnails.mimetype == "image/png" || thumbnails.mimetype == "image/jpg" || thumbnails.mimetype == "image/jpeg") {
                        filename2 = "thumbnails-"+uid+".png";
                        thumbnails.mv(root + "/public/upload/books/" + filename + "/thumbnails/" + filename2, function(err) {
                            if (err) {
                                return res.status(500).json({
                                    error: true,
                                    response: "Error When Upload Thumbnails!",
                                });
                            }
                        });
                    } else {
                        return res.status(500).json({
                            error: true,
                            response: "File Type Not Accept!",
                        });
                    }
                }

                var query   = `INSERT INTO flipbook.book (code, title, categories, status, thumbnail, file, sinopsis, publisher, year_publish, background, createdby, createdtime) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
                var result  = await sequelize.query(query, {
                    replacements: [code, title, categories, status, filename2, filename, sinopsis, publisher, yearPublish, filename1, decoded.code, now],
                    type: sequelize.QueryTypes.INSERT,
                });
                
                if(result){
                    var insertDetail = await tools.convertPDF(root + "/public/upload/books/" + filename + "/" + filename + ".pdf", filename, code, uid, decoded.code);

                    // if (insertDetail) {
                    let data = [code, title, categories, status, filename2, filename, sinopsis, publisher, yearPublish, filename1, decoded.code, now];
                    let log = await tools.addLog("Flipbook|Book", "Insert", JSON.stringify(data), decoded.code);
                    
                    if (log){
                        return res.status(200).json({
                            error: false,
                            response: "Add Data Success!",
                        });
                    }
                    // }
                }

                return res.status(500).json({
                    error: true,
                    response: "Add Data Failed. Please Try Again!",
                })
            } else {
                return res.status(500).json({
                    error: true,
                    response: "File Type Not Accept!",
                });
            }
        }
    } else {
        return res.status(500).json({
            error: true,
            response: "Books Cannot Empty!",
        });     
    }
};

exports.deleteBooks = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    // PARAMS
    const code = req.query.code;
    
    query       = `SELECT * FROM flipbook.book WHERE code = ?`;
    var result1 = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.SELECT,
    });
    
    let deleteFile = await tools.deleteFile(code);

    query  = `DELETE FROM flipbook.book WHERE code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });

    query  = `DELETE FROM flipbook.book_detail WHERE book_code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });

    query  = `DELETE FROM flipbook.book_attribute WHERE book_code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });

    query  = `DELETE FROM flipbook.book_comment WHERE book_code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });

    query  = `DELETE FROM flipbook.book_rating WHERE book_code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });
    

    if(result){
        let data    = result1;
        const log   = await tools.addLog("Flipbook|Book", "Delete", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Delete Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Delete Data Failed. Please Try Again!",
    })
};

exports.getBooksDetail = async (req, res) => {
    // GET JWT TOKEN
    // const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    // const decoded  = jwt_decode(jwttoken);

    // var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    // var result  = await sequelize.query(query, {
    //     replacements: [decoded.code, 'Admin', '1'],
    //     type: sequelize.QueryTypes.SELECT,
    // });

    // if (!result) {
    //     return res.status(500).json({
    //         error: true,
    //         response: "User Not Found/Not Active!",
    //     });
    // }

    const code  = req.query.code;
    const books = req.query.books;
    const pages = req.query.pages;
    var where  = "";

    if (code != undefined && code != "") {
        where += `AND a.code = '${code}'`;
    }
    
    if (books != undefined && books != "") {
        where += `AND a.book_code = '${books}'`;
    }
    
    if (pages != undefined && pages != "") {
        where += `AND a.pages = '${pages}'`;
    }

    query  = `SELECT b.file, b.background FROM flipbook.book AS b WHERE code = '${books}' LIMIT 1`;
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    let file = result[0].file;
    let background = result[0].background;

    query  = `SELECT a.* FROM flipbook.book_detail AS a WHERE 1=1 ${where} ORDER BY a.pages ASC`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        file: file,
        background: background,
        data: result
    });
};

exports.getBooksAttribute = async (req, res) => {
    // GET JWT TOKEN
    // const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    // const decoded  = jwt_decode(jwttoken);

    // var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    // var result  = await sequelize.query(query, {
    //     replacements: [decoded.code, 'Admin', '1'],
    //     type: sequelize.QueryTypes.SELECT,
    // });

    // if (!result) {
    //     return res.status(500).json({
    //         error: true,
    //         response: "User Not Found/Not Active!",
    //     });
    // }

    const code = req.query.code;
    const book = req.query.book;
    const pages = req.query.pages;
    const type = req.query.type;
    var where  = "";

    if (code != undefined && code != "") {
        where += `AND code = '${code}'`;
    }

    if (book != undefined && book != "") {
        where += `AND book_code = '${book}'`;
    }

    if (pages != undefined && pages != "") {
        where += `AND pages = '${pages}'`;
    }
    
    if (type != undefined && type != "") {
        where += `AND type = '${type}'`;
    }

    query  = `SELECT * FROM flipbook.book_attribute WHERE 1=1 ${where}`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.addBooksAttribute = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);
    // DECODE JWT TOKEN
    var decoded = jwt_decode(jwttoken);

    var code      = req.body.code;
    if (code == '') {
        code = await tools.getNotransactions("flipbook.book_attribute", "code", "ATR");
    } else {
        var query   = `DELETE FROM flipbook.book_attribute WHERE code = ?`;
        var result  = await sequelize.query(query, {
            replacements: [code],
            type: sequelize.QueryTypes.DELETE,
        });
    }
    const book      = req.body.book;
    const pages     = req.body.pages;
    const panjang   = req.body.panjang;
    const lebar     = req.body.lebar;
    const x         = req.body.x;
    const y         = req.body.y;
    const link      = req.body.link;
    const type      = req.body.type;
    const widthbook = req.body.widthbook;

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    var query   = `INSERT INTO flipbook.book_attribute (code, book_code, pages, width, height, x, y, link, type, widthbook) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
    var result  = await sequelize.query(query, {
        replacements: [code, book, pages, panjang, lebar, x, y, link, type, widthbook],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        let data    = [code, book, pages, panjang, lebar, x, y, link, type, widthbook];
        const log   = await tools.addLog("Flipbook|Book's Attribute", "Insert", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Add Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
};

exports.deleteBooksAttribute = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    // PARAMS
    const code = req.query.code;

    query       = `SELECT * FROM flipbook.book_attribute WHERE code = ?`;
    var result1 = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.SELECT,
    });

    query  = `DELETE FROM flipbook.book_attribute WHERE code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });
    
    if(result){
        let data    = result1;
        const log   = await tools.addLog("Flipbook|Book's Attribute", "Delete", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Delete Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Delete Data Failed. Please Try Again!",
    })
};

exports.getQuestion = async (req, res) => {
    // // GET JWT TOKEN
    // const jwttoken = JSON.stringify(req.headers.authorization);

    // // DECODE JWT TOKEN
    // const decoded  = jwt_decode(jwttoken);

    // var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    // var result  = await sequelize.query(query, {
    //     replacements: [decoded.code, 'Admin', '1'],
    //     type: sequelize.QueryTypes.SELECT,
    // });

    // if (!result) {
    //     return res.status(500).json({
    //         error: true,
    //         response: "User Not Found/Not Active!",
    //     });
    // }

    const code = req.query.code;
    var where  = "";

    if (code != undefined && code != "") {
        where += `AND code = '${code}'`;
    }

    query  = `SELECT a.* FROM flipbook.question AS a WHERE 1=1 ${where}`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.addQuestion = async (req, res) => {
    now = tools.getDateTimeNow();

    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);
    // DECODE JWT TOKEN
    var decoded = jwt_decode(jwttoken);

    
    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });
    
    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }
    
    const code     = await tools.getNotransactions("flipbook.question", "code", "QSN");
    const nama     = req.body.nama;
    const type     = req.body.type;
    const jenis    = req.body.jenis;

    var query   = `INSERT INTO flipbook.question (code, name, type, jenis, createdby, createdtime) VALUES(?, ?, ?, ?, ?, ?)`;
    var result  = await sequelize.query(query, {
        replacements: [code, nama, type, jenis, decoded.code, now],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        let data    = [code, nama, type, jenis, decoded.code, now];
        const log   = await tools.addLog("Flipbook|Question", "Insert", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Add Data Success!",
                code: code
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
};

exports.deleteQuestion = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    // PARAMS
    const code = req.query.code;

    query       = `SELECT * FROM flipbook.question AS a JOIN flipbook.question_detail AS b ON b.question_code = a.code JOIN flipbook.question_answer AS c ON c.question_code = a.code WHERE a.code = ?`;
    var result1 = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.SELECT,
    });

    query  = `DELETE FROM flipbook.question WHERE code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });

    query  = `DELETE FROM flipbook.question_detail WHERE question_code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });

    query  = `DELETE FROM flipbook.question_answer WHERE question_code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });
    
    if(result){
        let data    = result1;
        const log   = await tools.addLog("Flipbook|Question", "Delete", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Delete Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Delete Data Failed. Please Try Again!",
    })
};

exports.getQuestionDetail = async (req, res) => {
    // // GET JWT TOKEN
    // const jwttoken = JSON.stringify(req.headers.authorization);

    // // DECODE JWT TOKEN
    // const decoded  = jwt_decode(jwttoken);

    // var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    // var result  = await sequelize.query(query, {
    //     replacements: [decoded.code, 'Admin', '1'],
    //     type: sequelize.QueryTypes.SELECT,
    // });

    // if (!result) {
    //     return res.status(500).json({
    //         error: true,
    //         response: "User Not Found/Not Active!",
    //     });
    // }

    const code = req.query.code;
    const questionCode = req.query.questionCode;
    var where  = "";

    if (code != undefined && code != "") {
        where += `AND code = '${code}'`;
    }
    
    if (questionCode != undefined && questionCode != "") {
        where += `AND question_code = '${questionCode}'`;
    }

    query  = `SELECT a.*, (SELECT SUM(weight) FROM flipbook.question_detail WHERE 1=1 ${where}) AS weight_total, (SELECT COUNT(*) FROM flipbook.question_answer WHERE detail_code = a.code AND right_answer = 'true') AS rightAnswer FROM flipbook.question_detail AS a WHERE 1=1 ${where} ORDER BY runningnumber ASC`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.addQuestionDetail = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);
    // DECODE JWT TOKEN
    var decoded = jwt_decode(jwttoken);

    var code = req.body.code;
    const questionCode = req.body.questionCode;
    const question = req.body.question;
    const runningnumber = req.body.runningnumber;
    const weight = req.body.weight;
    const type = req.body.type;
    var filename = null;

    if (code == '') {
        code = await tools.getNotransactions("flipbook.question_detail", "code", "QSD");
    } else {
        var query   = `DELETE FROM flipbook.question_detail WHERE code = ?`;
        var result  = await sequelize.query(query, {
            replacements: [code],
            type: sequelize.QueryTypes.DELETE,
        });

        if(req.files){
            if(req.files.images){
                fs.rmSync(root + "/public/upload/question/" + questionCode.replaceAll("/", "") + "/" + code.replaceAll("/", "") + ".png", { recursive: true });
                filename = code.replaceAll("/", "");
            }
        }

        query   = `DELETE FROM flipbook.question_answer WHERE detail_code = ?`;
        result  = await sequelize.query(query, {
            replacements: [code],
            type: sequelize.QueryTypes.DELETE,
        });
    }

    query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    if (req.files) {
        let images = req.files.images != null ? req.files.images : null;

        if (images) {
            if (images.mimetype == "image/png" || images.mimetype == "image/jpg" || images.mimetype == "image/jpeg") {
                filename = code.replaceAll("/", "");
                images.mv(root + "/public/upload/question/" + questionCode.replaceAll("/", "") + "/" + filename + ".png", function(err) {
                    if (err) {
                        return res.status(500).json({
                            error: true,
                            response: "Error When Upload Images!",
                        });
                    }
                });
            } else {
                return res.status(500).json({
                    error: true,
                    response: "File Type Not Accept!",
                });
            }
        }
    }

    var query   = `INSERT INTO flipbook.question_detail (code, question_code, question, images, runningnumber, weight, type) VALUES(?, ?, ?, ?, ?, ?, ?)`;
    var result  = await sequelize.query(query, {
        replacements: [code, questionCode, question, filename, runningnumber, weight, type],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        let data    = [code, questionCode, question, filename, runningnumber, weight, type];
        const log   = await tools.addLog("Flipbook|Question Detail", "Insert", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Add Data Success!",
                code: code
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
};

exports.deleteQuestionDetail = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    
    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    // PARAMS
    const code = req.query.code;
    var questionCode = null;

    query       = `SELECT * FROM flipbook.question_detail WHERE code = ?`;
    var result1 = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.SELECT,
    });

    console.log(result1);

    questionCode = result1[0].question_code;
    
    if (result1[0].images != "" && result1[0].images != null) {
        fs.rmSync(root + "/public/upload/question/" + questionCode.replaceAll("/", "") + "/" + code.replaceAll("/", "") + ".png", { recursive: true });
    }

    query  = `DELETE FROM flipbook.question_detail WHERE code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });

    query  = `DELETE FROM flipbook.question_answer WHERE detail_code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });
    
    if(result){
        let data    = result1;
        const log   = await tools.addLog("Flipbook|Question Detail", "Delete", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Delete Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Delete Data Failed. Please Try Again!",
    })
};

exports.getQuestionAnswer = async (req, res) => {
    // // GET JWT TOKEN
    // const jwttoken = JSON.stringify(req.headers.authorization);

    // // DECODE JWT TOKEN
    // const decoded  = jwt_decode(jwttoken);

    // var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    // var result  = await sequelize.query(query, {
    //     replacements: [decoded.code, 'Admin', '1'],
    //     type: sequelize.QueryTypes.SELECT,
    // });

    // if (!result) {
    //     return res.status(500).json({
    //         error: true,
    //         response: "User Not Found/Not Active!",
    //     });
    // }

    const code = req.query.code;
    const detailCode = req.query.detailCode;
    var where  = "";

    if (code != undefined && code != "") {
        where += `AND code = '${code}'`;
    }
    
    if (detailCode != undefined && detailCode != "") {
        where += `AND detail_code = '${detailCode}'`;
    }

    query  = `SELECT a.* FROM flipbook.question_answer AS a WHERE 1=1 ${where}`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.addQuestionAnswer = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);
    // DECODE JWT TOKEN
    var decoded = jwt_decode(jwttoken);

    const code = await tools.getNotransactions("flipbook.question_answer", "code", "QSA");
    const detailCode = req.body.detailCode;
    const key_answer = req.body.key_answer;
    const value_answer = req.body.value_answer;
    const right_answer = req.body.right_answer;
    const question_code = req.body.question_code;

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    var query   = `INSERT INTO flipbook.question_answer (code, detail_code, key_answer, value_answer, right_answer, question_code) VALUES(?, ?, ?, ?, ?, ?)`;
    var result  = await sequelize.query(query, {
        replacements: [code, detailCode, key_answer, value_answer, right_answer, question_code],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        let data    = [code, detailCode, key_answer, value_answer, right_answer, question_code];
        const log   = await tools.addLog("Flipbook|Question Answer", "Insert", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Add Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
};

exports.deleteQuestionAnswer = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    // PARAMS
    const code = req.query.code;

    query       = `SELECT * FROM flipbook.question_answer AS c WHERE b.code = ?`;
    var result1 = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.SELECT,
    });

    query  = `DELETE FROM flipbook.question_answer WHERE code = ?`;
    result = await sequelize.query(query, {
        replacements: [code],
        type: sequelize.QueryTypes.DELETE,
    });
    
    if(result){
        let data    = result1;
        const log   = await tools.addLog("Flipbook|Question Answer", "Delete", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Delete Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Delete Data Failed. Please Try Again!",
    })
};

const generateUsername = (word1, word2) => {
    const suffix = [moment(date).tz('Asia/Bangkok').format('YYYY'), moment(date).tz('Asia/Bangkok').format('MM'), moment(date).tz('Asia/Bangkok').format('DD')];
    const prefix = ["user"];

    let suggestions = [];
    suggestions.push(`${word1}${word2}`);

    suffix.forEach((word) => {
        suggestions.push(`${word1}${word}${word2}`);
        suggestions.push(`${word1}${word}`);
        suggestions.push(`${word2}${word}`);
        suggestions.push(`${word1}${word2}${word}`);
    });

    prefix.forEach((word) => {
        suggestions.push(`${word1}${word}${word2}`);
        suggestions.push(`${word}${word1}`);
        suggestions.push(`${word}${word2}`);
        suggestions.push(`${word1}${word}${word2}`);
    });

    return suggestions;
};