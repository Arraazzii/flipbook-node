const { sequelize } = require('../models/db.js');
const { uuid }      = require('uuidv4');

const jwt_decode  = require("jwt-decode");
const express     = require('express');
const app         = express();
const secret      = require('../config/secret.js');
const tools       = require('./toolsController.js');

app.set('superSecret', secret.code);

exports.updateProfile = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    // Parameter
    const nama       = req.body.nama;
    const jk         = req.body.jeniskelamin;
    const minat      = req.body.minat;
    const temlahir   = req.body.tempatlahir;
    const tgllahir   = req.body.tanggallahir;
    const email      = req.body.email;
    const telepon    = req.body.telepon;
    const alamat     = req.body.alamat;
    const kecamatan  = req.body.kecamatan;
    const kelurahan  = req.body.kelurahan;
    const kodepos    = req.body.kodepos;
    const warga      = req.body.kewarganegaraan;
    const agama      = req.body.agama;
    const nikah      = req.body.statuspernikahan;
    
    const query  = `UPDATE mspencaker SET NamaPencaker = '${nama}', TempatLahir = '${temlahir}', TglLahir = '${tgllahir}', JenisKelamin = '${jk}', TypePekerjaan = '${minat}', Email = '${email}', Telepon = '${telepon}', Alamat = '${alamat}', KodePos = '${kodepos}', Kewarganegaraan = '${warga}', IDAgama = '${agama}', IDStatusPernikahan = '${nikah}' WHERE IDUser = '${decoded.id}' AND IDPencaker= '${decoded.idu}'`;
    const result = await sequelize.query(query);
    
    if(result){
        return res.status(200).json({
            error: false,
            response: "Update Data Success!",
        })
    };

    return res.status(200).json({
        error: true,
        response: "Update Data Failed. Please Try Again!",
    });
};

exports.updateCV = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    //Data Pendidikan
    const pendidikan = req.body.pendidikanterakhir;
    const jurusan    = req.body.jurusan;
    const skill      = req.body.skill;
    const jenisnilai = req.body.jenisnilai;
    const nilai      = req.body.nilai;
    const tahunlulus = req.body.tahunlulus;
    const keterangan = req.body.keterangan;

    //Jabatan yang diinginkan
    const jabatan = req.body.jabatan;
    const lokasi  = req.body.lokasi;
    const gaji    = req.body.gaji;

    const query  = `UPDATE mspencaker SET IDStatusPendidikan = '${pendidikan}', Jurusan = '${jurusan}', Keterampilan = '${skill}', NEMIPK = '${jenisnilai}', Nilai = '${nilai}', TahunLulus = '${tahunlulus}', Keterangan = '${keterangan}', IDPosisiJabatan = '${jabatan}', Lokasi = '${lokasi}', UpahYangDicari = '${gaji}' WHERE IDUser = '${decoded.id}' AND IDPencaker = '${decoded.idu}'`;
    const result = await sequelize.query(query);
    
    if(result){
        return res.status(200).json({
            error: false,
            response: "Update Data Success!",
        })
    };

    return res.status(200).json({
        error: true,
        response: "Update Data Failed. Please Try Again!",
    });
};

exports.getBahasa = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    const id   = req.query.id;
    var where  = "";
    if (id != undefined && id != "") {
        where += `AND IDBahasa = '${id}'`;
    }
    const query  = `SELECT * FROM msbahasa WHERE IDPencaker = ? ${where}`
    const result = await sequelize.query(query, {
        replacements: [decoded.idu],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(200).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.addBahasa = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);
    
    const id      = await tools.getNotransactions("msbahasa", "IDBahasa", 6);
    const bahasa  = req.body.bahasa;

    const query  = `INSERT INTO msbahasa (IDBahasa, IDPencaker, NamaBahasa) VALUES (?, ?, ?)`
    const result = await sequelize.query(query, {
        replacements: [id, decoded.idu, bahasa],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        return res.status(200).json({
            error: false,
            response: "Add Data Success!",
        })
    }

    return res.status(200).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
};

exports.updateBahasa = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);
    
    const id      = req.body.id;
    const bahasa  = req.body.bahasa;
    
    const query  = `UPDATE msbahasa SET NamaBahasa = '${bahasa}' WHERE IDPencaker = '${decoded.idu}' AND IDBahasa = '${id}'`;
    const result = await sequelize.query(query);
    if(result){
        return res.status(200).json({
            error: false,
            response: "Update Data Success!",
        })
    }

    return res.status(200).json({
        error: true,
        response: "Update Data Failed. Please Try Again!",
    })
};

exports.deleteBahasa = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    // PARAMS
    const id     = req.params.id;

    const query  = `DELETE FROM msbahasa WHERE IDBahasa = '${id}'`;
    const result = await sequelize.query(query);
    
    if(result){
        return res.status(200).json({
            error: false,
            response: "Delete Data Success!",
        })
    }

    return res.status(200).json({
        error: true,
        response: "Delete Data Failed. Please Try Again!",
    })
};

exports.getRiwayatLamaran = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    const limit  = req.query.limit;
    const offset = req.query.offset;

    const query  = `SELECT a.*, b.* FROM trlowonganmasuk as a JOIN mslowongan as b on b.IDLowongan = a.IDLowongan WHERE a.IDPencaker = ? LIMIT ? OFFSET ?`;
    const result = await sequelize.query(query, {
        replacements: [decoded.idu, parseInt(limit), parseInt(offset)],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(200).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.getPengalaman = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);
    const id   = req.query.id;
    var where  = "";
    if (id != undefined && id != "") {
        where += `AND IDPengalaman = '${id}'`;
    }
    const query  = `SELECT * FROM mspengalaman WHERE IDPencaker = ? ${where}`
    const result = await sequelize.query(query, {
        replacements: [decoded.idu],
        type: sequelize.QueryTypes.SELECT,
    });

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.addPengalaman = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);
    
    const id            = await tools.getNotransactions("mspengalaman", "IDPengalaman", 6);
    const jabatan       = req.body.jabatan;
    const pt            = req.body.perusahaan;
    const tglmasuk      = req.body.tglmasuk;
    const tglberhenti   = req.body.tglberhenti;
    const status        = req.body.status;

    const query  = `INSERT INTO mspengalaman (IDPengalaman, IDPencaker, Jabatan, UraianKerja, NamaPerusahaan, TglMasuk, TglBerhenti, IDPencakerTemp, StatusPekerjaan) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`
    const result = await sequelize.query(query, {
        replacements: [parseInt(id)+1, decoded.idu, jabatan, "", pt, tglmasuk, tglberhenti, decoded.username, status],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        return res.status(200).json({
            error: false,
            response: "Add Data Success!",
        })
    }

    return res.status(200).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
};

exports.updatePengalaman = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);
    
    const id            = req.body.id;
    const jabatan       = req.body.jabatan;
    const pt            = req.body.perusahaan;
    const tglmasuk      = req.body.tglmasuk;
    const tglberhenti   = req.body.tglberhenti;
    const status        = req.body.status;

    const query  = `UPDATE mspengalaman SET Jabatan = '${jabatan}', NamaPerusahaan = '${pt}', TglMasuk = '${tglmasuk}', TglBerhenti = '${tglberhenti}', StatusPekerjaan = '${status}' WHERE IDPengalaman = '${id}' AND IDPencaker = '${decoded.idu}'`;
    const result = await sequelize.query(query);
    
    if(result){
        return res.status(200).json({
            error: false,
            response: "UPDATE Data Success!",
        })
    }

    return res.status(200).json({
        error: true,
        response: "UPDATE Data Failed. Please Try Again!",
    })
};

exports.deletePengalaman = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    // PARAMS
    const id     = req.params.id;

    const query  = `DELETE FROM mspengalaman WHERE IDPengalaman = '${id}' AND IDPencaker = '${decoded.idu}'`;
    const result = await sequelize.query(query);

    if(result){
        return res.status(200).json({
            error: false,
            response: "Delete Data Success!",
        })
    }

    return res.status(200).json({
        error: true,
        response: "Delete Data Failed. Please Try Again!",
    })
};