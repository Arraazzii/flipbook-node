const { sequelize } = require('../models/db.js');

//Include Packages
const jwt         = require('jsonwebtoken');
const jwt_decode  = require("jwt-decode");
const express     = require('express');
const nodemailer  = require('nodemailer');
const app         = express();
const secret      = require('../config/secret.js');
const tools       = require('./toolsController.js');
const ejs         = require("ejs");
const path        = require("path");
const md5         = require("md5");

var root = path.dirname(require.main.filename || process.mainModule.filename);

app.set('superSecret', secret.code);
app.set('email', secret.email);
app.set('password', secret.password);

exports.login = async (req, res) => {
    const username  = req.body.username;
    const password  = md5(req.body.password);

    var query = `SELECT a.code, a.first_name, a.last_name, a.role, a.status FROM public.users AS a JOIN public.auth AS b ON b.code = a.code WHERE b.username = ? AND b.password = ? LIMIT 1`
    var result = await sequelize.query(query, {
        replacements: [username, password],
        type: sequelize.QueryTypes.SELECT,
    });

    var data = result[0];

    if (!data) {
        return res.status(200).json({
            error: true,
            response: "Account Not Found!",
        });
    } else {
        if (data.status == 0) {
            return res.status(200).json({
                error: true,
                response: "Account Not Activated!",
            });
        }
        // SET PAYLOAD JWT
        const payload = {
            code: data.code,
            username: username,
            password: password,
            first_name: data.first_name,
            last_name: data.last_name,
            role: data.role
        };

        // CREATE JWT 30 DAYS
        var token = jwt.sign(payload, app.get("superSecret"), {
            expiresIn: "30d",
        });

        const log   = await tools.addLog("Flipbook|Auth", "Login", JSON.stringify(payload), data.code);
            
        if (log){
            return res.status(200).json({
                error: false,
                response: "Login Success!",
                data: [{ token:token }],
            });
        }
    }
};

exports.register = async (req, res) => {

};

exports.changePassword = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND status = ?`
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }
    
    const username = req.body.username;
    const old_password = md5(req.body.old_password);
    const new_password = md5(req.body.new_password);

    query       = `SELECT * FROM public.auth WHERE code = ?`;
    var before  = await sequelize.query(query, {
        replacements: [decoded.code],
        type: sequelize.QueryTypes.SELECT,
    });

    query   = `SELECT * FROM public.auth WHERE username = ? AND password = ?`;
    result  = await sequelize.query(query, {
        replacements: [username, old_password],
        type: sequelize.QueryTypes.SELECT,
    });

    if (result.length < 1) {
        return res.status(200).json({
            error: true,
            response: "Wrong Username/Password!",
        });
    }

    query  = `UPDATE public.auth SET username = ?, password = ? WHERE code = ?`;
    result = await sequelize.query(query, {
        replacements: [username, new_password, decoded.code],
        type: sequelize.QueryTypes.UPDATE,
    });

    if(result){
        query       = `SELECT * FROM public.auth WHERE code = ?`;
        var after   = await sequelize.query(query, {
            replacements: [decoded.code],
            type: sequelize.QueryTypes.SELECT,
        });

        let data = {
            before:before,
            after:after
        };
        const log   = await tools.addLog("Flipbook|Change Password", "Update", JSON.stringify(data), decoded.code);
            
        if (log){
            return res.status(200).json({
                error: false,
                response: "Update Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Update Data Failed. Please Try Again!",
    })
};

exports.refreshtoken = (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    let decodednow  = jwt_decode(jwttoken);
    let header      = req.headers.authorization.split(" ");
    let token       = header[1];
    if (req.headers.authorization) {
        jwt.verify(token, app.get("superSecret"), (err, decoded) => {
            if (err) {
                if (err.message == "jwt expired") {
                    // CREATE PAYLOAD
                    const payload = {
                        id: decodednow.id,
                        type: decodednow.type
                    };

                    // SIGN NEW JWT IN 30 DAYS
                    var newtoken = jwt.sign(payload, app.get("superSecret"), {
                        expiresIn: "30d",
                    });

                    return res.status(200).json({
                        error: false,
                        data: [{ token: newtoken }],
                        response: "Token Refresh",
                    });
                }

                return res.status(500).json({
                    error: true,
                    response: "Failed Refresh Token!",
                });
            }
            return res.status(200).json({
                error: false,
                response: "Not Refresh!",
            });
        });
    } else {
        return res.status(403).json({
            error: true,
            response: "No token provided",
        });
    }
};

exports.forgotPassword = async (req, res) => {
    const username  = req.body.username; // Perusahaan Berisi Informasi NIB
    const email     = req.body.email;

    var query  = `SELECT IDJenisUser as jenis, IDUser FROM msuser WHERE Username = ? LIMIT 1`
    var result = await sequelize.query(query, {
        replacements: [username],
        type: sequelize.QueryTypes.SELECT,
    });

    if (result.length == 0) {
        return res.status(200).json({
            error: true,
            response: "Username Not Found!",
        });
    }
    
    if(result[0].jenis == '000001'){
        var query2  = `SELECT Email FROM msperusahaan WHERE Email = ? LIMIT 1`
        var result2 = await sequelize.query(query2, {
            replacements: [email],
            type: sequelize.QueryTypes.SELECT,
        });
        if (result2.length == 0) {
            return res.status(200).json({
                error: true,
                response: "Email Not Found!",
            });
        }
    }else{
        var query2  = `SELECT Email FROM mspencaker WHERE Email = ? LIMIT 1`
        var result2 = await sequelize.query(query2, {
            replacements: [email],
            type: sequelize.QueryTypes.SELECT,
        });
        if (result2.length == 0) {
            return res.status(200).json({
                error: true,
                response: "Email Not Found!",
            });
        }
    }

    var newpassword = makeid(10)
    var queryUPDATE   = `UPDATE msuser set password=? WHERE IDUser = ?`
    resultupdate  = await sequelize.query(queryUPDATE, {
        replacements: [newpassword, result[0].IDUser],
        type: sequelize.QueryTypes.UPDATE,
    });

    if (result) {
        var transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            secure: true,
            port: 465,
            auth: {
                user: app.get("email"),
                pass: app.get("password"),
            },
        });

        ejs.renderFile(
            root + "/views/mail/forgot.ejs",
            {newpassword: newpassword},
            function (err, data) {
                if (err) {
                    return res.status(200).json({
                        error: true,
                        response: "Failed Sent Email!",
                    });
                } else {
                    let mainOptions = {
                        from: "BKOL <noreply@bkol.depok.go.id>",
                        to: email,
                        subject: "Password Baru Akun Anda",
                        html: data,
                    };
                    transporter.sendMail(mainOptions, function (err, info) {
                        if (err) {
                            return res.status(200).json({
                                error: true,
                                response: "Failed Sent Email!",
                            });
                        } else {
                            return res.status(200).json({
                                error: false,
                                response: "Password Dikirimkan Ke Email!",
                            });
                        }
                    });
                }
            }
        );
    }
};

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}