const { sequelize } = require('../models/db.js');
const { uuid }      = require('uuidv4');

const jwt_decode  = require("jwt-decode");
const express     = require('express');
const app         = express();
const secret      = require('../config/secret.js');
const tools       = require('./toolsController.js');
const fcmnotif       = require('./firebaseController.js');


app.set('superSecret', secret.code);

exports.getLowongan = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);
    
    var query  = `SELECT * FROM mslowongan WHERE IDPerusahaan='${decoded.idu}' ORDER BY TglBerakhir DESC`;
    var result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(200).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
}

exports.updateProfile = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    // Parameter
    const nama       = req.body.nama;
    const bidang     = req.body.bidang;
    const email      = req.body.email;
    const telepon    = req.body.telepon;
    const alamat     = req.body.alamat;
    const provinsi   = req.body.provinsi;
    const kabupaten  = req.body.kabupaten;
    const kecamatan  = req.body.kecamatan;
    const kelurahan  = req.body.kelurahan;
    const kodepos    = req.body.kodepos;
    
    const query  = `UPDATE msperusahaan SET NamaPerusahaan = '${nama}', IDBidangPerusahaan = '${bidang}', Email = '${email}', Telepon = '${telepon}', Alamat = '${alamat}', KodePos = '${kodepos}' WHERE IDPerusahaan = '${decoded.idu}' AND IDUser = '${decoded.id}'`;
    const result = await sequelize.query(query);
    
    if(result){
        return res.status(200).json({
            error: false,
            response: "Update Data Success!"
        })
    };

    return res.status(200).json({
        error: true,
        response: "Update Data Failed. Please Try Again!"
    });
};

exports.updatePIC = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    // Parameter
    const nama       = req.body.namapic;
    const jabatan    = req.body.jabatanpic;
    const email      = req.body.emailpic;
    const telepon    = req.body.teleponpic;
    
    const query  = `UPDATE msperusahaan SET NamaPemberiKerja = '${nama}', JabatanPemberiKerja = '${jabatan}', EmailPemberiKerja = '${email}', TeleponPemberiKerja = '${telepon}' WHERE IDPerusahaan = '${decoded.idu}' AND IDUser = '${decoded.id}'`;
    const result = await sequelize.query(query);
    
    if(result){
        return res.status(200).json({
            error: false,
            response: "Update Data Success!"
        })
    };

    return res.status(200).json({
        error: true,
        response: "Update Data Failed. Please Try Again!"
    });
};

exports.addLowongan = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);
    
    const id                = await tools.getNotransactions("mslowongan", "IDLowongan", 6);
    const nama              = req.body.nama;
    const rincian           = req.body.rincian;
    const tugas             = req.body.tugas;
    const jabatan           = req.body.jabatan;
    const lokasi            = req.body.lokasi;
    const laki              = req.body.laki;
    const perempuan         = req.body.perempuan;
    const minpendidikan     = req.body.minpendidikan;
    const statusupah        = req.body.statusupah;
    const hubkerja          = req.body.hubkerja;
    const jurusan           = req.body.jurusan;
    const pengalaman        = req.body.pengalaman
    const batasusia         = req.body.batasusia;
    const syarat            = req.body.syarat;
    const gaji              = req.body.gaji;
    const jamkerja          = req.body.jamkerja
    const tglawal           = req.body.tglawal;
    const tglakhir          = req.body.tglakhir;
    const seleksimulai      = req.body.seleksimulai;
    const seleksiakhir      = req.body.seleksiakhir;
    const pengumumanawal    = req.body.pengumumanawal;
    const pengumumanakhir   = req.body.pengumumanakhir;
    const idkeahlian   = req.body.idkeahlian;

    var today     = new Date();
    const dd      = String(today.getDate()).padStart(2, '0');
    const mm      = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    const yyyy    = today.getFullYear();
    today         = yyyy + '/' + mm + '/' + dd + " " + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

    const query  = `INSERT INTO mslowongan (IDLowongan, IDPerusahaan, IDPosisiJabatan, IDStatusPendidikan, IDJenisPengupahan, IDStatusHubunganKerja, NamaLowongan, UraianPekerjaan, UraianTugas, Penempatan, BatasUmur, JmlPria, JmlWanita, Jurusan, Pengalaman, SyaratKhusus, GajiPerbulan, JamKerjaSeminggu, RegisterDate, tglBerlaku, tglBerakhir, tglberlakutahapan, tglberakhirtahapan, tglberlakupenerimaan, tglberakhirpenerimaan, IDKeahlian, flyer) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`
    const result = await sequelize.query(query, {
        replacements: [id, decoded.idu, jabatan, minpendidikan, statusupah, hubkerja, nama, rincian, tugas, lokasi, batasusia, laki, perempuan, jurusan, pengalaman, syarat, gaji, jamkerja, today, tglawal, tglakhir, seleksimulai, seleksiakhir, pengumumanawal, pengumumanakhir, idkeahlian, 'img-lowongan.png'],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        return res.status(200).json({
            error: false,
            response: "Add Data Success!",
            id: id
        })
    }

    return res.status(200).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
};

exports.updateLowongan = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);
    
    const id                = req.body.id;
    const nama              = req.body.nama;
    const rincian           = req.body.rincian;
    const tugas             = req.body.tugas;
    const jabatan           = req.body.jabatan;
    const lokasi            = req.body.lokasi;
    const laki              = req.body.laki;
    const perempuan         = req.body.perempuan;
    const minpendidikan     = req.body.minpendidikan;
    const statusupah        = req.body.statusupah;
    const hubkerja          = req.body.hubkerja;
    const jurusan           = req.body.jurusan;
    const pengalaman        = req.body.pengalaman
    const batasusia         = req.body.batasusia;
    const syarat            = req.body.syarat;
    const gaji              = req.body.gaji;
    const jamkerja          = req.body.jamkerja
    const tglawal           = req.body.tglawal;
    const tglakhir          = req.body.tglakhir;
    const seleksimulai      = req.body.seleksimulai;
    const seleksiakhir      = req.body.seleksiakhir;
    const pengumumanawal    = req.body.pengumumanawal;
    const pengumumanakhir   = req.body.pengumumanakhir;
    const idkeahlian   = req.body.idkeahlian;

    var today     = new Date();
    const dd      = String(today.getDate()).padStart(2, '0');
    const mm      = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    const yyyy    = today.getFullYear();
    today         = yyyy + '/' + mm + '/' + dd + " " + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

    const query  = `UPDATE mslowongan SET IDPosisiJabatan = ?, IDStatusPendidikan = ?, IDJenisPengupahan = ?, IDStatusHubunganKerja = ?, NamaLowongan = ?, UraianPekerjaan = ?, UraianTugas = ?, Penempatan = ?, BatasUmur = ?, JmlPria = ?, JmlWanita = ?, Jurusan = ?, Pengalaman = ?, SyaratKhusus = ?, GajiPerbulan = ?, JamKerjaSeminggu = ?, RegisterDate = ?, tglBerlaku = ?, tglBerakhir = ?, tglberlakutahapan = ?, tglberakhirtahapan = ?, tglberlakupenerimaan = ?, tglberakhirpenerimaan = ?, IDKeahlian = ? WHERE IDLowongan = ? AND IDPerusahaan = ?`;
    const result = await sequelize.query(query, {
        replacements: [jabatan, minpendidikan, statusupah, hubkerja, nama, rincian, tugas, lokasi, batasusia, laki, perempuan, jurusan, pengalaman, syarat, gaji, jamkerja, today, tglawal, tglakhir, seleksimulai, seleksiakhir, pengumumanawal, pengumumanakhir, idkeahlian, id, decoded.idu],
        type: sequelize.QueryTypes.UPDATE,
    });
    
    if(result){
        return res.status(200).json({
            error: false,
            response: "Update Data Success!",
            id : id
        })
    };

    return res.status(200).json({
        error: true,
        response: "Update Data Failed. Please Try Again!",
    });
};

exports.deleteLowongan = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    // PARAMS
    const id     = req.params.id;

    const query  = `DELETE FROM mslowongan WHERE IDLowongan = '${id}' AND IDPerusahaan = '${decoded.idu}'`;
    const result = await sequelize.query(query);

    const query2  = `DELETE FROM trlowonganmasuk WHERE IDLowongan = '${id}'`;
    const result2 = await sequelize.query(query2);

    
    if(result2){
        return res.status(200).json({
            error: false,
            response: "Delete Data Success!",
        })
    }

    return res.status(200).json({
        error: true,
        response: "Delete Data Failed. Please Try Again!",
    })
};

exports.getPencaker = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    const idLowongan = req.query.idlowongan;
    const status = req.query.status;

    var wherestatus =''
    
    if(status !== ''){
        wherestatus += ` AND StatusLowongan ='${status}'`
    }

    const query  = `SELECT * FROM trlowonganmasuk WHERE IDLowongan = '${idLowongan}' ${wherestatus} ORDER BY IDLowonganMasuk DESC`
    const result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    const querycountlaki  = `SELECT COALESCE(COUNT(IDLowonganMasuk), 0) as total FROM trlowonganmasuk WHERE IDLowongan = '${idLowongan}' AND JenisKelamin='0' ${wherestatus}`
    const resultcountlaki = await sequelize.query(querycountlaki, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    const querycountperempuan  = `SELECT COALESCE(COUNT(IDLowonganMasuk), 0) as total FROM trlowonganmasuk WHERE IDLowongan = '${idLowongan}' AND JenisKelamin='1' ${wherestatus}`
    const resultcountperempuan = await sequelize.query(querycountperempuan, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    const queryjudul  = `SELECT NamaLowongan FROM mslowongan WHERE IDLowongan = '${idLowongan}' ORDER BY IDLowongan DESC LIMIT 1`
    const resultjudul = await sequelize.query(queryjudul, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(200).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
        laki: resultcountlaki[0].total,
        perempuan: resultcountperempuan[0].total,
        judul: resultjudul[0].NamaLowongan
    });
};

exports.prosesPencaker = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    const id        = req.body.id;
    const pencaker  = req.body.pencaker;
    const idlowongan  = req.body.idlowongan;

    const query  = `UPDATE trlowonganmasuk SET StatusLowongan = '${id}' WHERE IDLowongan = '${idlowongan}' AND IDPencaker = '${pencaker}'`
    console.log(query)
    const result = await sequelize.query(query);

    if(id == 1){
        var statusUpdate = 'Diterima'
    }else{
        var statusUpdate = 'Ditolak'
    }

    var getPerusahaan= `SELECT b.IDUser, b.NamaPerusahaan FROM mslowongan as a JOIN msperusahaan as b ON b.IDPerusahaan = a.IDPerusahaan WHERE a.IDLowongan='${idlowongan}'`
    var resultPerusahaan = await sequelize.query(getPerusahaan, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    var getPencaker= `SELECT b.IDUser FROM trlowonganmasuk as a JOIN mspencaker as b ON b.IDPencaker = a.IDPencaker WHERE a.IDLowongan='${idlowongan}'`
    var resultPencaker = await sequelize.query(getPencaker, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if(result){
        await tools.insertNotification(`Lamaran Anda ${statusUpdate}`, `Lamaran anda di ${resultPerusahaan[0].NamaPerusahaan} Telah ${statusUpdate}`, resultPencaker[0].IDUser)
        await fcmnotif.pushNotif(`Lamaran Anda ${statusUpdate}`, `Lamaran anda di ${resultPerusahaan[0].NamaPerusahaan} Telah ${statusUpdate}`, resultPencaker[0].IDUser)
        return res.status(200).json({
            error: false,
            response: "Update Data Success!"
        })
    };

    return res.status(200).json({
        error: true,
        response: "Update Data Failed. Please Try Again!"
    });
};

exports.getProfilePencaker = async (req, res) => {
    const final = {};
    // GET JWT TOKEN
    // const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    // const decoded  = jwt_decode(jwttoken);
    var id = req.query.id;
    var idlowongan = req.query.idlowongan;
    if(id == ""){
        return res.status(200).json({
            error: true,
            response: "ID Notfound!",
        });
    }

    var table   = "mspencaker"

    var query  = `SELECT * FROM ${table} WHERE IDPencaker = ?`
    var result = await sequelize.query(query, {
        replacements: [id],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(200).json({
            error: true,
            response: "User Not Found!",
        });
    }

    final.profile = result;

    var query2  = `SELECT * FROM mspengalaman WHERE IDPencaker = ?`
    var result2 = await sequelize.query(query2, {
        replacements: [id],
        type: sequelize.QueryTypes.SELECT,
    });

    final.pengalaman = result2;

    const query3  = `SELECT StatusLowongan FROM trlowonganmasuk WHERE IDLowongan = '${idlowongan}' AND IDPencaker = '${id}' ORDER BY IDLowonganMasuk DESC`
    const result3 = await sequelize.query(query3, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: final,
        statusLowongan: result3[0].StatusLowongan
    });
};