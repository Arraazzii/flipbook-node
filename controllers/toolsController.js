const { sequelize } = require('../models/db.js');
const d             = Date(Date.now());
// const now          = d.toString();

var jwt_decode      = require("jwt-decode");
const express       = require('express');
const app           = express();
const secret        = require('../config/secret.js');
const moment        = require('moment');
const path          = require("path");
const root          = path.dirname(require.main.filename || process.mainModule.filename);
const fs            = require('fs');
const pdf2img       = require('pdf-img-convert');

app.set('superSecret', secret.code);


exports.getNotransactions = async (table, column, prefix, digit = 7) => {
    
    var id   = 0;
    var date = new Date();
    var text = getDate(date);
    var code = `${text}/${prefix}/`;

    const query  = `SELECT MAX(${column}) AS jumlah FROM ${table} WHERE ${column} LIKE '%${code}%'`;
    const result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });
    
    id = addZero(id + 1, digit);
    if (result[0].jumlah) {
        let terakhir = result[0].jumlah.split('/');
        id = addZero(parseInt(terakhir[2]) + 1, digit);
    }

    code = `${text}/${prefix}/${id}`; //20000101/PFX/0000001 20 digit

    return code;
};

exports.getMasterCode = async (table, column, prefix, digit = 7) => {
    
    var id   = 0
    var code = `${prefix}/`;

    const query  = `SELECT MAX(${column}) AS jumlah FROM ${table} WHERE ${column} LIKE '%${code}%'`;
    const result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });


    id = addZero(id + 1, digit);
    if (result[0].jumlah) {
        let terakhir = result[0].jumlah.split('/');
        id = addZero(parseInt(terakhir[1]) + 1, digit);
    }

    code = `${prefix}/${id}`; //PFX/0000001 20 digit

    return code;
};

exports.checkUsername = async (username) => {
    const query  = `SELECT COUNT(*) AS jumlah FROM public.auth WHERE username = '${username}'`;
    const result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (result[0].jumlah > 0) {
        return false;
    }

    return true;
};

exports.insertNotification = async (notification, description, id_user) => {
    const queryinsert  = `INSERT INTO fcm_notification (notification, description, id_user, created_at) VALUES (?, ?, ?, ?)`
    const resultinsert = await sequelize.query(queryinsert, {
        replacements: [notification, description, id_user, moment(new Date()).format('YYYY-MM-DD HH:mm:ss')],
        type: sequelize.QueryTypes.INSERT,
    });

    return resultinsert;
};

//Activity
exports.getLog = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);
    
    var query   = `SELECT * FROM public.users WHERE code = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    const code   = req.query.code;
    var where  = "";

    if (code != undefined && code != "") {
        where += `AND code = '${code}'`;
    }

    query  = `SELECT a.*, b.first_name, b.last_name FROM public.log_activity AS a LEFT JOIN public.users AS b ON b.code = a.createdby WHERE 1=1 ${where} ORDER BY createdtime DESC`;
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.addLog = async (modul, action, data, userby) => {
    let now = this.getDateTimeNow();
    const code   = await this.getNotransactions("public.log_activity", "code", "LOG");

    var query   = `INSERT INTO public.log_activity (code, modul, action, data, createdby, createdtime) VALUES(?, ?, ?, ?, ?, ?)`;
    var result  = await sequelize.query(query, {
        replacements: [code, modul, action, data, userby, now],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if (result) return true;
    return false;
}

exports.convertPDF = async (link, folder, book_code, uid, user) => {
    var that = this;
    (async function () {
        pdfArray = await pdf2img.convert(link);
        var code_detail = null;
        var pages = null;
        var filename = null;

        if (!fs.existsSync(root + "/public/upload/books/" + folder + "/detail")){
            fs.mkdirSync(root + "/public/upload/books/" + folder + "/detail", { recursive: true });
        }

        for (i = 0; i < pdfArray.length; i++){
            code_detail = await that.getNotransactions("flipbook.book_detail", "code", "BOD");
            pages = i + 1;
            filename = "Books-" + uid + pages + ".png";
            fs.writeFile(root + "/public/upload/books/" + folder + "/detail/" + filename, pdfArray[i], function (error) {
                if (error) { console.error("Error: " + error); }
            });

            query   = `INSERT INTO flipbook.book_detail (code, pages, book_code, file) VALUES(?, ?, ?, ?)`;
            result  = await sequelize.query(query, {
                replacements: [code_detail, pages, book_code, filename],
                type: sequelize.QueryTypes.INSERT,
            });

            // if (result) {
            //     let data = [code_detail, pages, book_code, filename];
            //     let log = that.addLog("Flipbook|Book Detail", "Insert", JSON.stringify(data), user);
            // }
        }

        return true;
    })();
}

exports.deleteFile = async (book) => {
    var query   = `SELECT file AS filename FROM flipbook.book WHERE code = ? LIMIT 1`
    var result  = await sequelize.query(query, {
        replacements: [book],
        type: sequelize.QueryTypes.SELECT,
    });

    if (result) {   
        fs.rmSync(root + "/public/upload/books/" + result[0].filename, { recursive: true });
        return true;
    }

    return false;
}

exports.getDateTimeNow = () => {
    let date_time = new Date();

    // get current date
    // adjust 0 before single digit date
    let date = ("0" + date_time.getDate()).slice(-2);

    // get current month
    let month = ("0" + (date_time.getMonth() + 1)).slice(-2);

    // get current year
    let year = date_time.getFullYear();

    // get current hours
    let hours = ("0" + date_time.getHours()).slice(-2);

    // get current minutes
    let minutes = ("0" + date_time.getMinutes()).slice(-2);

    // get current seconds
    let seconds = ("0" + date_time.getSeconds()).slice(-2);

    let hasil = year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;

    return hasil;
}

const addZero = (data, digit) => {
    do {
        data = "0" + data;
    }
    while (data.length < digit);

    return data;
};

const getDate = (today) => {
    var dd      = today.getDate();
    var mm      = today.getMonth()+1; //January is 0!
    var yyyy    = today.getFullYear();
    var hour    = today.getHours();
    var minutes = today.getMinutes();

    if( dd < 10 )  { dd = '0' + dd } 
    if( mm < 10 )  { mm = '0' + mm } 
    if( minutes < 10 ){ minutes = '0' + minutes }

    return yyyy+mm+dd;
}