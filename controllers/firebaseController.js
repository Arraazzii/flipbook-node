var express         = require('express');
var app             = express();
var serviceAccount  = require('../config/depokbkolapps-firebase-adminsdk-xh6td-34c7a95341.json');
var admin           = require("firebase-admin");
const jwt_decode  = require("jwt-decode");
const { sequelize } = require('../models/db.js');
const moment = require('moment')
var FCM = require('fcm-node');
var serverKey = 'AAAAeveNwzY:APA91bHdlX5-c4L8bQd-O-wGuRjpOf2kQ3g4y4SiQlLFNLEnjlW5lZtpCyRCBYRjIh5uBcmPwBmmO5XUWFN1aFdvAHDdQeKzGnW5hXra7T-prmz7SvOqBPa0nAE8AWT-6C6DYtgW56PW';
var fcm = new FCM(serverKey);

exports.adduserfcm = async(req,res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);
    
    const token  = req.body.token;

    const query  = `INSERT INTO fcm (idUser, type, token, created_at) VALUES (?, ?, ?, ?)`
    const result = await sequelize.query(query, {
        replacements: [decoded.id, decoded.type, token, moment(new Date()).format('YYYY-MM-DD HH:mm:ss')],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        return res.status(200).json({
            error: false,
            response: "Add Data Success!",
        })
    }

    return res.status(200).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
}

exports.deleteuserfcm = async(req,res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);
    const token  = req.params.token;

    const query  = `DELETE FROM fcm where token='${token}' AND idUser=${decoded.id}`
    const result = await sequelize.query(query);
    
    if(result){
        return res.status(200).json({
            error: false,
            response: "Delete Data Success!",
        })
    }

    return res.status(200).json({
        error: true,
        response: "Delete Data Failed. Please Try Again!",
    })
}

exports.pushNotif = async (title, body, id) => {
    var query  = `SELECT * FROM fcm WHERE IDUser = ?`
    var result = await sequelize.query(query, {
        replacements: [id],
        type: sequelize.QueryTypes.SELECT,
    });
    if(result.length > 0){
        result.map(async(value, id) => {
            var message = {
                to: value.token, 
                notification: {
                    title: title, 
                    body: body 
                }
            };

            await fcm.send(message, function(err, response){
                if (err) {
                    console.log('send')   
                } else {
                    console.log(response)
                }
            });
        })

        return "Send";
    }else{
        return "User Not Found";
    }
};
