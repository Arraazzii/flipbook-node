const { sequelize } = require('../models/db.js');

const jwt_decode  = require("jwt-decode");
const express     = require('express');
const app         = express();
const secret      = require('../config/secret.js');
const tools       = require('./toolsController.js');
const fcmnotif    = require('./firebaseController.js');

app.set('superSecret', secret.code);

exports.getProfiles = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    const code = req.query.code;
    var where  = "";

    if (code != undefined && code != "") {
        where += `AND code = '${code}'`;
    }

    query  = `SELECT * FROM public.users WHERE 1=1 ${where}`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.getMaster = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role = ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    query  = `SELECT * FROM flipbook.master LIMIT 1`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.getVersion = async (req, res) => {
    return res.status(200).json({
        android: "1.0.0",
        ios: "1.0.0",
        website: "1.0.0",
        api: "1.0.0"
    });
};

exports.updatePassword = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    const passwordLama = req.body.passwordLama;
    const passwordBaru = req.body.passwordBaru;

    if (passwordLama != decoded.password) {
        return res.status(200).json({
            error: true,
            response: "Old Password Didnt Match!",
        })
    }
    
    const query  = `UPDATE msuser SET password = ? WHERE IDUser = ?`;
    const result = await sequelize.query(query, {
        replacements: [passwordBaru, decoded.id],
        type: sequelize.QueryTypes.UPDATE,
    });
    
    if(result){
        return res.status(200).json({
            error: false,
            response: "Update Data Success!",
        })
    }

    return res.status(200).json({
        error: true,
        response: "Update Data Failed. Please Try Again!",
    })
};

exports.getNotif = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);
    
    var query  = `SELECT * FROM fcm_notification WHERE id_user='${decoded.id}' ORDER BY created_at DESC LIMIT 100`;
    var result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(200).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
}