const { sequelize } = require('../models/db.js');

var jwt_decode  = require("jwt-decode");
var express     = require('express');
var app         = express();
var secret      = require('../config/secret.js');
var moment      = require('moment')

const { uuid }  = require('uuidv4');
const d         = Date(Date.now());
// const now      = d.toString();

//Get Tools from toolsController
const tools = require('./toolsController.js');
var now   = tools.getDateTimeNow();

app.set('superSecret', secret.code);

//Student
exports.answerQuestion = async (req, res) => {
    now = tools.getDateTimeNow();

    // // GET JWT TOKEN
    // const jwttoken = JSON.stringify(req.headers.authorization);

    // // DECODE JWT TOKEN
    // const decoded  = jwt_decode(jwttoken);

    // var query   = `SELECT * FROM public.users WHERE code = ? AND role != ? and status = ?`;
    // var result  = await sequelize.query(query, {
    //     replacements: [decoded.code, 'Admin', '1'],
    //     type: sequelize.QueryTypes.SELECT,
    // });

    // if (!result) {
    //     return res.status(500).json({
    //         error: true,
    //         response: "User Not Found/Not Active!",
    //     });
    // }

    const code     = await tools.getNotransactions("flipbook.answer_question", "code", "AQS");
    const user     = req.body.user;
    const question = req.body.question;
    const answer   = req.body.answer;
    const grade    = req.body.grade;
    const parent   = req.body.parent;

    var query   = `INSERT INTO flipbook.answer_question (code, user_code, question_code, answer, grade, createdby, createdtime, parent) VALUES(?, ?, ?, ?, ?, ?, ?, ?)`;
    var result  = await sequelize.query(query, {
        replacements: [code, user, question, answer, grade, user, now, parent],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        let data    = [code, user, question, answer, grade, user, now, parent];
        const log   = await tools.addLog("Flipbook|Answer Question", "Insert", JSON.stringify(data), user);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Add Data Success!",
                code: code
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
}

//Teacher
exports.gradingAnswer = async (req, res) => {
    now = tools.getDateTimeNow();

    // // GET JWT TOKEN
    // const jwttoken = JSON.stringify(req.headers.authorization);

    // // DECODE JWT TOKEN
    // const decoded  = jwt_decode(jwttoken);

    // var query   = `SELECT * FROM public.users WHERE code = ? AND role != ? and status = ?`;
    // var result  = await sequelize.query(query, {
    //     replacements: [decoded.code, 'Admin', '1'],
    //     type: sequelize.QueryTypes.SELECT,
    // });

    // if (!result) {
    //     return res.status(500).json({
    //         error: true,
    //         response: "User Not Found/Not Active!",
    //     });
    // }

    const code     = req.body.code;
    const grade    = req.body.grade;
    const user     = req.body.user;

    var query   = `UPDATE flipbook.answer_question SET grade = ?, updatedby = ?, updatedtime = ? WHERE code = ?`;
    var result  = await sequelize.query(query, {
        replacements: [grade, user, now, code],
        type: sequelize.QueryTypes.UPDATE,
    });
    
    if(result){
        let data    = [grade, user, now, code];
        const log   = await tools.addLog("Flipbook|Grading Answer Question", "Update", JSON.stringify(data), user);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Update Data Success!",
                code: code
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Update Data Failed. Please Try Again!",
    })
}

exports.resetAnswer = async (req, res) => {
    // // GET JWT TOKEN
    // const jwttoken = JSON.stringify(req.headers.authorization);

    // // DECODE JWT TOKEN
    // const decoded  = jwt_decode(jwttoken);

    // var query   = `SELECT * FROM public.users WHERE code = ? AND role != ? and status = ?`
    // var result  = await sequelize.query(query, {
    //     replacements: [decoded.code, 'Admin', '1'],
    //     type: sequelize.QueryTypes.SELECT,
    // });

    // if (!result) {
    //     return res.status(500).json({
    //         error: true,
    //         response: "User Not Found/Not Active!",
    //     });
    // }

    // PARAMS
    const user = req.query.user;
    const teacher = req.query.teacher;
    const question = req.query.question;

    query       = `SELECT * FROM flipbook.answer_question WHERE user_code = ? AND parent = ?`;
    var result1 = await sequelize.query(query, {
        replacements: [user, question],
        type: sequelize.QueryTypes.SELECT,
    });

    query  = `DELETE FROM flipbook.answer_question WHERE user_code = ? AND parent = ?`;
    result = await sequelize.query(query, {
        replacements: [user, question],
        type: sequelize.QueryTypes.DELETE,
    });

    if (result) {
        let data    = result1;
        const log   = await tools.addLog("Flipbook|Reset Answer", "Delete", JSON.stringify(data), teacher);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Delete Data Success!",
            });
        }
    }   

    return res.status(500).json({
        error: true,
        response: "Delete Data Failed. Please Try Again!",
    })
}

//General
exports.getAnswerQuestion = async (req, res) => {
    // // GET JWT TOKEN
    // const jwttoken = JSON.stringify(req.headers.authorization);

    // // DECODE JWT TOKEN
    // const decoded  = jwt_decode(jwttoken);
    
    // var query   = `SELECT * FROM public.users WHERE code = ? AND role != ? and status = ?`;
    // var result  = await sequelize.query(query, {
    //     replacements: [decoded.code, 'Admin', '1'],
    //     type: sequelize.QueryTypes.SELECT,
    // });

    // if (!result) {
    //     return res.status(500).json({
    //         error: true,
    //         response: "User Not Found/Not Active!",
    //     });
    // }

    const code      = req.query.code;
    const question  = req.query.question;
    const user      = req.query.user;
    const parent    = req.query.parent;
    var where  = "";

    if (code != undefined && code != "") {
        where += `AND a.code = '${code}'`;
    }
    
    if (question != undefined && question != "") {
        where += `AND a.question_code = '${question}'`;
    }

    if (user != undefined && user != "") {
        where += `AND a.user_code = '${user}'`;
    }
    
    if (parent != undefined && parent != "") {
        where += `AND b.question_code = '${parent}'`;
    }

    query  = `SELECT a.*, b.question_code AS parent FROM flipbook.answer_question AS a LEFT JOIN flipbook.question_detail AS b ON b.code = a.question_code WHERE 1=1 ${where}`;
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.getUser = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);
    
    var query   = `SELECT * FROM public.users WHERE code = ? AND role != ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    const code      = req.query.code;
    const role      = req.query.role;
    const username  = req.query.username;
    var where  = "";

    if (code != undefined && code != "") {
        where += `AND a.code = '${code}'`;
    }
    
    if (role != undefined && role != "") {
        where += `AND role = '${role}'`;
    }

    if (username != undefined && username != "") {
        where += `AND b.username = '${username}'`;
    }

    query  = `SELECT a.*, b.username FROM public.users AS a JOIN public.auth AS b ON b.code = a.code WHERE 1=1 ${where}`;
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.getClass = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);
    
    var query   = `SELECT * FROM public.users WHERE code = ? AND role != ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    const code = req.query.code;
    var where = "";

    if (code != undefined && code != "") {
        where += `AND code = '${code}'`;
    }

    query  = `SELECT * FROM flipbook.class WHERE 1=1 ${where}`;
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.getUserClass = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role != ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    const classs = req.query.classs;
    const role = req.query.role;
    const user = req.query.user;
    var where = "";

    if (classs != undefined && classs != "") {
        where += `AND a.class_code = '${classs}'`;
    }
    
    if (role != undefined && role != "") {
        where += `AND b.role = '${role}'`;
    }

    if (user != undefined && user != "") {
        where += `AND a.user_code = '${user}'`;
    }

    query  = `SELECT a.*, b.first_name, b.last_name FROM flipbook.class_user AS a JOIN public.users AS b ON b.code = a.user_code WHERE 1=1 ${where}`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.getBookClass = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role != ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    const classs = req.query.classs;
    var where = "";

    if (classs != undefined && classs != "") {
        where += `AND a.class_code = '${classs}'`;
    }

    query  = `SELECT a.* FROM flipbook.class_book AS a WHERE 1=1 ${where}`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.getCategories = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role != ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    const code = req.query.code;
    var where  = "";

    if (code != undefined && code != "") {
        where += `AND code = '${code}'`;
    }

    query  = `SELECT * FROM flipbook.categories WHERE 1=1 ${where}`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.getRatings = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role != ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    const book = req.query.book;
    const user = req.query.user;
    var where  = "";

    if (book != undefined && book != "") {
        where += ` AND book_code = '${book}'`;
    }

    if (user != undefined && user != "") {
        where += ` AND createdby = '${user}'`;
    }

    query  = `SELECT a.*, b.first_name, b.last_name FROM flipbook.book_rating AS a LEFT JOIN public.users AS b ON b.code = a.createdby WHERE 1=1${where}`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.addRatings = async (req, res) => {
    now = tools.getDateTimeNow();

    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);
    // DECODE JWT TOKEN
    var decoded = jwt_decode(jwttoken);

    const code          = await tools.getNotransactions("flipbook.book_rating", "code", "BRT");
    const rating        = req.body.rating;
    const book          = req.body.book;
    const descriptions  = req.body.descriptions;

    var query   = `SELECT * FROM public.users WHERE code = ? AND role != ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    var query   = `INSERT INTO flipbook.book_rating (code, rating, book_code, descriptions, createdby, createdtime) VALUES(?, ?, ?, ?, ?, ?)`;
    var result  = await sequelize.query(query, {
        replacements: [code, rating, book, descriptions, decoded.code, now],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        let data    = [code, rating, book, descriptions, decoded.code, now];
        const log   = await tools.addLog("Flipbook|Rating", "Insert", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Add Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
};

exports.getComments = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role != ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    const parent = req.query.parent;
    var where  = "";

    if (parent != undefined && parent != "") {
        where += `AND parent_code = '${parent}'`;
    }

    query  = `SELECT a.*, b.first_name, b.last_name FROM flipbook.book_comment AS a LEFT JOIN public.users AS b ON b.code = a.createdby WHERE 1=1 ${where}`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.addComments = async (req, res) => {
    now = tools.getDateTimeNow();
    
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);
    // DECODE JWT TOKEN
    var decoded = jwt_decode(jwttoken);

    const code          = await tools.getNotransactions("flipbook.book_comment", "code", "CMN");
    const parent        = req.body.parent;
    const book          = req.body.book;
    const comment       = req.body.comment;

    var query   = `SELECT * FROM public.users WHERE code = ? AND role != ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    var query   = `INSERT INTO flipbook.book_comment (code, parent_code, book, comment, createdby, createdtime) VALUES(?, ?, ?, ?, ?)`;
    var result  = await sequelize.query(query, {
        replacements: [code, parent, book, comment, decoded.code, now],
        type: sequelize.QueryTypes.INSERT,
    });
    
    if(result){
        let data    = [code, parent, book, comment, decoded.code, now];
        const log   = await tools.addLog("Flipbook|Comment", "Insert", JSON.stringify(data), decoded.code);
        
        if (log){
            return res.status(200).json({
                error: false,
                response: "Add Data Success!",
            });
        }
    }

    return res.status(500).json({
        error: true,
        response: "Add Data Failed. Please Try Again!",
    })
};

exports.getBooks = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role != ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    const code = req.query.code;
    const categories = req.query.categories;
    var where  = "";

    if (code != undefined && code != "") {
        where += `AND a.code = '${code}'`;
    }
    
    if (categories != undefined && categories != "") {
        where += `AND a.categories = '${categories}'`;
    }

    query  = `SELECT a.*, b.name AS category_name FROM flipbook.book AS a JOIN flipbook.categories AS b ON b.code = a.categories WHERE 1=1 ${where}`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};

exports.getBooksVideo = async (req, res) => {
    // GET JWT TOKEN
    const jwttoken = JSON.stringify(req.headers.authorization);

    // DECODE JWT TOKEN
    const decoded  = jwt_decode(jwttoken);

    var query   = `SELECT * FROM public.users WHERE code = ? AND role != ? and status = ?`;
    var result  = await sequelize.query(query, {
        replacements: [decoded.code, 'Admin', '1'],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "User Not Found/Not Active!",
        });
    }

    const code = req.query.code;
    const book = req.query.book;
    var where  = "";

    if (code != undefined && code != "") {
        where += `AND code = '${code}'`;
    }

    if (book != undefined && book != "") {
        where += `AND book_code = '${book}'`;
    }

    query  = `SELECT * FROM flipbook.book_video WHERE 1=1 ${where}`
    result = await sequelize.query(query, {
        replacements: [],
        type: sequelize.QueryTypes.SELECT,
    });

    if (!result) {
        return res.status(500).json({
            error: true,
            response: "Data Not Found!",
        });
    }

    return res.status(200).json({
        error: false,
        response: "Success Get Data!",
        data: result,
    });
};