'use strict'

// const fs = require('fs')
// const path = require('path')
const Sequelize = require('sequelize')
// const basename = path.basename(__filename)
const env = process.env.NODE_ENV || 'development'
const config = require('../config/db.js')
const db = {}

const db2 = new Sequelize(config.db2.DB, config.db2.USER, config.db2.PASSWORD, {
  host: config.db2.HOST,
  dialect: config.db2.dialect,
  logging: config.db2.logging,
  operatorsAliases: config.db2.operatorsAliases,

  pool: {
    max: config.db2.pool.max,
    min: config.db2.pool.min,
    acquire: config.db2.pool.acquire,
    idle: config.db2.pool.idle
  }
})

db.sequelize = db2

module.exports = db
