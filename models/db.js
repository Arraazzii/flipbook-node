'use strict'

// const fs = require('fs')
// const path = require('path')
const Sequelize = require('sequelize')
// const basename = path.basename(__filename)
const env = process.env.NODE_ENV || 'development'
const config = require('../config/db.js')
const db = {}

const db1 = new Sequelize(config.db1.DB, config.db1.USER, config.db1.PASSWORD, {
  host: config.db1.HOST,
  dialect: config.db1.dialect,
  logging: config.db1.logging,
  operatorsAliases: config.db1.operatorsAliases,

  pool: {
    max: config.db1.pool.max,
    min: config.db1.pool.min,
    acquire: config.db1.pool.acquire,
    idle: config.db1.pool.idle
  }
})

db.sequelize = db1

module.exports = db
