const jwt       = require("jsonwebtoken");
const express   = require("express");
const app       = express();
const secret    = require("../config/secret.js");

app.set("superSecret", secret.code);

module.exports = (app) => {
    const admin      = require("../controllers/adminController.js");
    const user       = require("../controllers/userController.js");
    const auth       = require("../controllers/authController.js");
    const utilities  = require("../controllers/utilitiesController.js");
    const tools      = require("../controllers/toolsController.js");
    const firebase   = require("../controllers/firebaseController.js");

    //ADMIN
        //USER
        app.get("/node/admin/user", admin.getUser);
        app.post("/node/admin/user/add", tokencheck, admin.addUser);
        app.post("/node/admin/user/addBulk", tokencheck, admin.addUserBulk);
        app.put("/node/admin/user/update", tokencheck, admin.updateUser);
        app.delete("/node/admin/user/delete", tokencheck, admin.deleteUser);

        //CLASS
        app.get("/node/admin/class", admin.getClass);
        app.post("/node/admin/class/add", tokencheck, admin.addClass);
        app.put("/node/admin/class/update", tokencheck, admin.updateClass);
        app.delete("/node/admin/class/delete", tokencheck, admin.deleteClass);

        //USER CLASS
        app.get("/node/admin/class/getUser", admin.getUserClass);
        app.post("/node/admin/class/addUser", tokencheck, admin.addUserClass);
        app.delete("/node/admin/class/deleteUser", tokencheck, admin.deleteUserClass);

        //BOOK CLASS
        app.get("/node/admin/class/getBook", admin.getBookClass);
        app.post("/node/admin/class/addBook", tokencheck, admin.addBookClass);
        app.delete("/node/admin/class/deleteBook", tokencheck, admin.deleteBookClass);

        //CATEGORIES
        app.get("/node/admin/categories", tokencheck, admin.getCategories);
        app.post("/node/admin/categories/add", tokencheck, admin.addCategories);
        app.put("/node/admin/categories/update", tokencheck, admin.updateCategories);
        app.delete("/node/admin/categories/delete", tokencheck, admin.deleteCategories);

        //RATINGS
        app.get("/node/admin/ratings", tokencheck, admin.getRatings);
        app.post("/node/admin/ratings/add", tokencheck, admin.addRatings);
        app.delete("/node/admin/ratings/delete", tokencheck, admin.deleteRatings);

        //COMMENTS
        app.get("/node/admin/comment", tokencheck, admin.getComments);
        app.post("/node/admin/comment/add", tokencheck, admin.addComments);
        app.delete("/node/admin/comment/delete", tokencheck, admin.deleteComments);

        //BOOKS
        app.get("/node/admin/books", tokencheck, admin.getBooks);
        app.post("/node/admin/books/add", tokencheck, admin.addBooks);
        // app.put("/node/admin/books/update", tokencheck, admin.updateBooks);
        app.delete("/node/admin/books/delete", tokencheck, admin.deleteBooks);
        app.get("/node/admin/books/attribute", admin.getBooksAttribute);
        app.post("/node/admin/books/attribute/add", tokencheck, admin.addBooksAttribute);
        app.delete("/node/admin/books/attribute/delete", tokencheck, admin.deleteBooksAttribute);

        //BOOKS Detail
        app.get("/node/admin/books/detail", admin.getBooksDetail);

        //QUESTION
        app.get("/node/admin/question", admin.getQuestion);
        app.post("/node/admin/question/add", tokencheck, admin.addQuestion);
        app.delete("/node/admin/question/delete", tokencheck, admin.deleteQuestion);
        app.get("/node/admin/question/detail", admin.getQuestionDetail);
        app.post("/node/admin/question/detail/add", tokencheck, admin.addQuestionDetail);
        app.delete("/node/admin/question/detail/delete", tokencheck, admin.deleteQuestionDetail);
        app.get("/node/admin/question/answer", admin.getQuestionAnswer);
        app.post("/node/admin/question/answer/add", tokencheck, admin.addQuestionAnswer);
        app.delete("/node/admin/question/answer/delete", tokencheck, admin.deleteQuestionAnswer);

    // USER
        //Student
        app.post("/node/student/answerQuestion", user.answerQuestion);
        //Teacher
        app.put("/node/teacher/gradingAnswer", user.gradingAnswer);
        app.delete("/node/teacher/resetAnswer", user.resetAnswer);
        //General
        app.get("/node/user/getAnswerQuestion", user.getAnswerQuestion);
        app.get("/node/user/user", tokencheck, user.getUser);
        app.get("/node/user/class", tokencheck, user.getClass);
        app.get("/node/user/class/getUser", tokencheck, user.getUserClass);
        app.get("/node/user/class/getBook", tokencheck, user.getBookClass);
        app.get("/node/user/categories", tokencheck, user.getCategories);
        app.get("/node/user/rating", tokencheck, user.getRatings);
        app.post("/node/user/rating/add", tokencheck, user.addRatings);
        app.get("/node/user/comment", tokencheck, user.getComments);
        app.post("/node/user/comment/add", tokencheck, user.addComments);
        app.get("/node/user/books", tokencheck, user.getBooks);
        app.get("/node/user/books/video", tokencheck, user.getBooksVideo);
    
    // AUTH
        app.post("/node/auth/login", auth.login);
        app.post("/node/auth/changePassword", auth.changePassword);

    // Utilities
        app.get("/node/utilities/master", tokencheck, utilities.getMaster);
        app.get("/node/utilities/version", tokencheck, utilities.getVersion);
        app.get("/node/utilities/profiles", tokencheck, utilities.getProfiles);
        app.get("/node/utilities/notification", tokencheck, utilities.getNotif);

    // Tools
        //Log Activity
        app.get("/node/tools/log", tokencheck, tools.getLog);
        app.post("/node/tools/log/add", tokencheck, tools.addLog);

    //FIREBASE
        app.post("/api/fcm", tokencheck, firebase.adduserfcm);
        app.post("/api/fcm/push", tokencheck, firebase.pushNotif);
        app.delete("/api/fcm/:token", tokencheck, firebase.deleteuserfcm);
};

const tokencheck = (req, res, next) => {
    if (req.headers.authorization) {
        let header  = req.headers.authorization.split(" ");
        let token   = header[1];
        if (token) {
            jwt.verify(token, app.get("superSecret"), (err, decoded) => {
                if (err) {
                    return res.status(401).send({
                        error: true,
                        response: "Failed to authenticate token",
                    });
                } else {
                    req.decoded = decoded;
                    next();
                }
            });
        } else {
            return res.status(401).send({
                error: true,
                response: "No token provided",
            });
        }
    } else {
        return res.status(401).send({
            error: true,
            response: "No token provided",
        });
    }
};
